#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
## Fixes for Ostap
#
#  This file is a part of 
#  <a href="http://cern.ch/lhcb-comp/Analysis/Bender/index.html">Bender project</a>
#  <b>``Python-based Interactive Environment for Smart and Friendly 
#   Physics Analysis''</b>
#
#  The package has been designed with the kind help from
#  Pere MATO and Andrey TSAREGORODTSEV. 
#  And it is based on the 
#  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
#  ``C++ ToolKit for Smart and Friendly Physics Analysis''
#
#  By usage of this code one clearly states the disagreement 
#  with the smear campaign of Dr.O.Callot et al.: 
#  ``No Vanya's lines are allowed in LHCb/Gaudi software.''
#
#  @author Vanya BELYAEV  Ivan.Belyaev@itep.ru
#  @date   2016-05-29
#
# =============================================================================
"""Fixes for Ostap 
 
This file is a part of BENDER project:
``Python-based Interactive Environment for Smart and Friendly Physics Analysis''

The project has been designed with the kind help from
Pere MATO and Andrey TSAREGORODTSEV. 

And it is based on the 
LoKi project: ``C++ ToolKit for Smart and Friendly Physics Analysis''

By usage of this code one clearly states the disagreement 
with the smear campaign of Dr.O.Callot et al.: 
``No Vanya's lines are allowed in LHCb/Gaudi software.''

"""
# =============================================================================
__author__  = 'Vanya BELYAEV Ivan.Belyaev@itep.ru'
__date__    = "2016-05-29"
__version__ = "$Revision: 202441 $"
__all__     = () 
# =============================================================================
## logging
# =============================================================================
from Ostap.Logger import getLogger 
if '__main__' == __name__ : logger = getLogger ( 'Ostap.Fixes' )
else                      : logger = getLogger ( __name__ )
# =============================================================================
import ROOT, cppyy
cpp = cppyy.makeNamespace('')
# =============================================================================
# First, suppress RiiFit banner 
# =============================================================================
ROOT.gEnv.SetValue('RooFit.Banner',"0")  ## does not work...
# suppress RooFit banner 
from Ostap.Utils import mute
with mute( True , True ) : 
    v = ROOT.RooRealVar()
    del v 
# =============================================================================
_fixes = 0 
# =============================================================================
## @class FakeClass
#  Some ``Fake class''
#  @author Vanya BELYAEV  Ivan.Belyaev@itep.ru
#  @date   2016-05-29
class FakeClass(object) :
    """Some ``Fake class''
    """
    def __init__    ( self , name = '' ,  *args , **kwargs ) :
        self.name = name 
        logger.error( "Fake class %s is created!" % name )
    def __getattr__ ( self , key ) :
            raise ArrtibuteError, "Attribute %s for fake class %s " % ( key , self.name )

## if not hasattr ( cpp.Gaudi.Math      , 'Sech' ) :
##     _fixes += 1 
##     cpp.Gaudi.Math.Sech      = FakeClass
##     logger.warning('Add fake class for cpp.Gaudi.Math.Sech' )    

## if not hasattr ( cpp.Analysis.Models , 'Sech' ) :
##     _fixes += 1 
##     cpp.Analysis.Models.Sech = FakeClass
##     logger.warning('Add fake class for cpp.Analysis.Models.Sech' )

## if not hasattr ( cpp.Gaudi.Math      , 'Swanson' ) :
##     _fixes += 1 
##     cpp.Gaudi.Math.Swanson     = FakeClass
##     logger.warning('Add fake class for cpp.Gaudi.Math.Swanson' )

## if not hasattr ( cpp.Analysis.Models , 'Swanson' ) :
##     _fixes += 1 
##     cpp.Analysis.Models.Swanson = FakeClass
##     logger.warning('Add fake class for cpp.Analysis.Models.Swanson' )

## if not hasattr ( cpp.Gaudi.Math      , 'PseudoVoigt' ) :
##     _fixes += 1 
##     cpp.Gaudi.Math.PseudoVoigt     = FakeClass
##     logger.warning('Add fake class for cpp.Gaudi.Math.PseudoVoigt' )

## if not hasattr ( cpp.Analysis.Models , 'PseudoVoigt' ) :
##     _fixes += 1 
##     cpp.Analysis.Models.PseudoVoigt = FakeClass
##     logger.warning('Add fake class for cpp.Analysis.Models.PseudoVoigt' )

## if not hasattr ( cpp.Gaudi.Math      , 'Logistic' ) :
##     _fixes += 1 
##     cpp.Gaudi.Math.Logistic     = FakeClass
##     logger.warning('Add fake class for cpp.Gaudi.Math.Logistic' )

## if not hasattr ( cpp.Analysis.Models , 'Logistic' ) :
##     _fixes += 1 
##     cpp.Analysis.Models.Logistic = FakeClass
##     logger.warning('Add fake class for cpp.Analysis.Models.Logistic' )

## if not hasattr ( cpp.Gaudi.Math      , 'BernsteinDualBasis' ) :
##     _fixes += 1 
##     cpp.Gaudi.Math.BernsteinDualBasis  = FakeClass
##     logger.warning('Add fake class for cpp.Gaudi.Math.BernsteinDualBasis' )

## if not hasattr ( cpp.Gaudi.Math      , 'BernsteinEven' ) :
##     _fixes += 1 
##     cpp.Gaudi.Math.BernsteinEven  = FakeClass
##     logger.warning('Add fake class for cpp.Gaudi.Math.BernsteinEven' )

## if not hasattr ( cpp.Gaudi.Math      , 'PositiveEven' ) :
##     _fixes += 1 
##     cpp.Gaudi.Math.PositiveEven  = FakeClass
##     logger.warning('Add fake class for cpp.Gaudi.Math.PositiveEven' )


if not hasattr ( cpp.Gaudi.Math      , 'Bernstein3D' ) :
    _fixes += 1 
    cpp.Gaudi.Math.Bernstein3D  = FakeClass
    logger.warning('Add fake class for cpp.Gaudi.Math.Bernstein3D' )

if not hasattr ( cpp.Gaudi.Math      , 'Bernstein3DSym' ) :
    _fixes += 1 
    cpp.Gaudi.Math.Bernstein3DSym  = FakeClass
    logger.warning('Add fake class for cpp.Gaudi.Math.Bernstein3DSym' )

if not hasattr ( cpp.Gaudi.Math      , 'Bernstein3DMix' ) :
    _fixes += 1 
    cpp.Gaudi.Math.Bernstein3DMix  = FakeClass
    logger.warning('Add fake class for cpp.Gaudi.Math.Bernstein3DMix' )

if not hasattr ( cpp.Gaudi.Math      , 'Positive3D' ) :
    _fixes += 1 
    cpp.Gaudi.Math.Positive3D  = FakeClass
    logger.warning('Add fake class for cpp.Gaudi.Math.Positive3D' )

if not hasattr ( cpp.Gaudi.Math      , 'Positive3DSym' ) :
    _fixes += 1 
    cpp.Gaudi.Math.Positive3DSym  = FakeClass
    logger.warning('Add fake class for cpp.Gaudi.Math.Positive3DSym' )

if not hasattr ( cpp.Gaudi.Math      , 'Positive3DMix' ) :
    _fixes += 1 
    cpp.Gaudi.Math.Positive3DMix  = FakeClass
    logger.warning('Add fake class for cpp.Gaudi.Math.Positive3DMix' )


if not hasattr ( cpp.Analysis.Models , 'Poly3DPositive' ) :
    _fixes += 1 
    cpp.Gaudi.Math.Poly3DPositive  = FakeClass
    logger.warning('Add fake class for cpp.Gaudi.Math.Poly3DPositive' )

if not hasattr ( cpp.Analysis.Models , 'Poly3DSymPositive' ) :
    _fixes += 1 
    cpp.Gaudi.Math.Poly3DSymPositive  = FakeClass
    logger.warning('Add fake class for cpp.Gaudi.Math.Poly3DSymPositive' )

if not hasattr ( cpp.Analysis.Models , 'Poly3DMixPositive' ) :
    _fixes += 1 
    cpp.Gaudi.Math.Poly3DMixPositive  = FakeClass
    logger.warning('Add fake class for cpp.Gaudi.Math.Poly3DMixPositive' )

## try :
    
##     _HI_1D = cpp.Gaudi.Math.HistoInterpolation.interpolate_1D
##     _HI_2D = cpp.Gaudi.Math.HistoInterpolation.interpolate_2D
##     _HI_3D = cpp.Gaudi.Math.HistoInterpolation.interpolate_3D
##     _h1    = ROOT.TH1D( '_XXX','',10,0,1)
##     _v     = _HI_1D ( _h1 , 0.1 , 1 , True , False , False )
    
## except TypeError :
    
##     def _interpolate_1D_ ( h1 , x , tx , edges , *args ) :
##         """ Interpolation of 1D histogram """ 
##         return _H1_1D ( h1 , x , tx ,   edges )
    
##     def _interpolate_2D_ ( h2 , x , y     , tx , ty      , *args ) :
##         """ Interpolation of 2D histogram """         
##         return _H1_2D ( h2 , x , y , tx , ty )
    
##     def _interpolate_3D_ ( h3 , x , y , z , tx , ty , tz , *args ) :
##         """ Interpolation of 3D histogram """         
##         return _H1_3D ( h3 , x , y , z , tx , ty , tz )
    
##     cpp.Gaudi.Math.HistoInterpolation.interpolate_1D = _interpolate_1D_ 
##     cpp.Gaudi.Math.HistoInterpolation.interpolate_2D = _interpolate_2D_ 
##     cpp.Gaudi.Math.HistoInterpolation.interpolate_3D = _interpolate_3D_
    
##     logger.warning( 'Fix histogram interpolation issue (ignore edges, extrapolation & density flags)' )
##     _fixes += 1
    
##     del _HI_1D
##     del _HI_2D
##     del _HI_3D


    
if _fixes : logger.info  ('%d fixes are applied' % _fixes )
else      : logger.debug ('No fixes are applied'          )

# =============================================================================
if '__main__' == __name__ :
    
    import Ostap.Line
    logger.info ( __file__  + '\n' + Ostap.Line.line  ) 
    logger.info ( 80*'*'   )
    logger.info ( __doc__  )
    logger.info ( 80*'*' )
    logger.info ( ' Author  : %s' %         __author__    ) 
    logger.info ( ' Version : %s' %         __version__   ) 
    logger.info ( ' Date    : %s' %         __date__      )
    logger.info ( ' Symbols : %s' %  list ( __all__     ) )
    logger.info ( 80*'*' ) 
    
# =============================================================================
# The END 
# =============================================================================
