#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
## @file TestGeneric1D.py
#
#  test for Generic1D_pdf
#
#  @author Vanya BELYAEV Ivan.Belyaeve@itep.ru
#  @date 2017-08-11
# =============================================================================
"""Test for Generic1D_pdf
"""
# =============================================================================
__version__ = "$Revision:"
__author__  = "Vanya BELYAEV Ivan.Belyaev@itep.ru"
__date__    = "2014-05-10"
__all__     = ()  ## nothing to be imported 
# =============================================================================
import ROOT, random
from   Ostap.PyRoUts import *
from   Ostap.Utils   import rooSilent 
# =============================================================================
# logging 
# =============================================================================
from AnalysisPython.Logger import getLogger
if '__main__' == __name__  or '__builtin__' == __name__ : 
    logger = getLogger ( 'Ostap.TestGeneric1D' )
else : 
    logger = getLogger ( __name__ )
# =============================================================================
logger.info ( 'Test for Generic1D_pdf from Analysis/Ostap')
# =============================================================================
from Ostap.FitBasic import Fit1D, Generic1D_pdf 

## make simple test mass 
pt       = ROOT.RooRealVar ( 'test_pT' , 'Some test pT' , 0 , 6)

## book very simple data set
varset   = ROOT.RooArgSet  ( pt  )
dataset  = ROOT.RooDataSet ( dsID() , 'Test Data set-0' , varset )  

ns = 90000
nb = 10000
while ns > len(dataset) :
    v = random.expovariate ( 1/5.0 )
    if v in pt :
        pt.value = v 
        dataset.add ( varset )
        
while ns+nb > len(dataset) :
    v = random.expovariate ( 1/0.5 )
    if v in pt :
        pt.value =  v 
        dataset.add ( varset ) 
        
# =============================================================================
## 1) construct the signal component using bare Roofit machinery
# =============================================================================
tau_s       = ROOT.RooRealVar     ( 'tau_s'       , 'tau-parameter for signal' , -10 , 0     )
signal_bare = ROOT.RooExponential ( 'signal_bare' , 'signal component'         , pt  , tau_s )

# =============================================================================
## 2) construct the background component using bare Roofit machinery
# =============================================================================
delta_tau   = ROOT.RooRealVar     ( 'delta_tau'   , 'delta(tau)-parameter'     , -10 , 0     )
tau_b       = tau_s.sum ( delta_tau , name = 'tau_b' , title = 'tau-parameter for backgroud' )
bkg_bare    = ROOT.RooExponential ( 'background_bare' , 'backgrond component' , pt  , tau_b  )

# =============================================================================
# 3) optional step: convert them to Ostap 
signal_ostap = Generic1D_pdf ( pdf = signal_bare , xvar = pt , name = 'signal_ostap' )
bkg_ostap    = Generic1D_pdf ( pdf = bkg_bare    , xvar = pt , name = 'bkg_ostap' )

# =============================================================================
## 4') build the final model 
# =============================================================================

## Build the model from "Ostap"-components
model1 = Fit1D  ( signal = signal_ostap , background = bkg_ostap , name   = 'M1' )

# =============================================================================
## 4") build the final model 
# =============================================================================

## build the model from raw-components  
model2 = Fit1D  ( signal = signal_bare , background = bkg_bare , name = 'M2' , xvar = pt )

## put some reasonable starting values 
for m in ( model1 , model2 ) :
    model1.S = 100000
    model1.B =   8000
tau_s    .value = -0.25 
delta_tau.value = -1.95

##  use regular ('Ostap') component
r1,f1 = model1.fitTo ( dataset ,  draw = True , silent = True )

##  use regular ('Ostap') component
r2,f2 = model2.fitTo ( dataset ,  draw = True , silent = True )

logger.info ( 'Model1: 1/tau_s %s, 1/tau_b %s ' % ( 1/r1.tau_s , 1/r1.sum( 'tau_s','delta_tau') ) )
logger.info ( 'Model2: 1/tau_s %s, 1/tau_b %s ' % ( 1/r2.tau_s , 1/r2.sum( 'tau_s','delta_tau') ) )

# =============================================================================
# The END 
# =============================================================================
