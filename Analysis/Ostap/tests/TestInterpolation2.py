#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ============================================================================= 
# @file test_histo_interpolation.py
# Test module for ostap/histos/histos.py
# - It tests interpolation for 1,2&3D histograms 
# ============================================================================= 
""" Test module for ostap/histos/histos.py
- It tests interpolation for 1,2&3D histograms 
"""
# ============================================================================= 
__author__ = "Ostap developers"
__all__    = () ## nothing to import 
# ============================================================================= 
import ROOT, random
# =============================================================================
# logging 
# =============================================================================
from Ostap.Logger import getLogger
if '__main__' == __name__  or '__builtin__' == __name__ : 
    logger = getLogger ( 'test_histo_interpolation' )
else : 
    logger = getLogger ( __name__ )
# =============================================================================
logger.info ( 'Test for 1,2&3D-histogram interpolation')
# =============================================================================

import ROOT, random
import Ostap.HistoDeco
from   Ostap.Core import hID, SE, VE 

h1 = ROOT.TH1D ( hID() , '', 5 , 0 , 1 )
h2 = ROOT.TH2D ( hID() , '', 5 , 0 , 1 , 5 , 0 , 1 ) 
h3 = ROOT.TH3D ( hID() , '', 5 , 0 , 1 , 5 , 0 , 1 , 5 , 0 , 1 ) 

fun1 = lambda x         : 0.1+x**3
fun2 = lambda x, y      : 0.1+x**3+y**2+x*y
fun3 = lambda x, y , z  : 0.1+x**3+y**2+x*y*z+z*x 

h1 += fun1
h2 += fun2
h3 += fun3


random.seed ( 1234567890 ) 

h1 *= VE( 1 , random.uniform(0.01,0.05)**2 )
h2 *= VE( 1 , random.uniform(0.01,0.05)**2 )
h3 *= VE( 1 , random.uniform(0.01,0.05)**2 )


# =============================================================================
##  test interpolation for 1D-historgams 
def  test_1D1 () :
    
    N = 100 
    for i in range(N) :   
        x  = random.uniform(0,1)
        vf = fun1  ( x )
        for t in ( 0 , 1 , 2 , 3 ) :
            for edges in ( False , True ) :
                for extrapolate in ( False , True ) :
                    key = t , edges , extrapolate
                    vh  = h1 (  x , interpolate = t , edges = edges  , extrapolate = extrapolate ) 
                    logger.info ( '1D: Key:%-30s x=%.3f h=%s' % ( key , x , vh.toString("(%.4f+-%.4f)") ) )  
                    
# =============================================================================
##  test interpolation for 2D-historgams 
def test_2D1() :

    N2 = 10
    for i in range(N2) :   
        x  = random.uniform(0,1)
        for j in range(N2) :
            y  = random.uniform(0,1)        
            for tx in ( 0 , 1 , 2 , 3 ) :
                for ty in ( 0 , 1 , 2 , 3 ) :                
                    for edges in ( True , ) :                  ## NB! 
                        for extrapolate in ( False , ) :       ## NB! 
                            key = tx, ty , edges , extrapolate                        
                            vh  = h2 ( x , y , interpolate = (tx , ty)  ,
                                       edges = edges  , extrapolate = extrapolate  )
                            
                            logger.info ( '2D: Key:%-30s x,y=(%.3f,%.3f) h=%s' % ( key , x , y , vh.toString("(%.4f+-%.4f)") ) )  
                            
# =============================================================================
##  test interpolation for 3D-historgams 
def test_3D1() :

    N3 = 10
    for i in range(N3) :   
        x  = random.uniform(0,1)
        for j in range(N3) :
            y  = random.uniform(0,1)
            for k in range(N3) :
                z  = random.uniform(0,1)
                for tx in ( 0 , 1 , 2 , 3 ) :
                    for ty in ( 0 , 1 , 2 , 3 ) :
                        for tz in ( 0 , 1 , 2 , 3 ) :                        
                            for edges in ( True , )  :            ## NB!!!
                                for extrapolate in ( False , ) :  ## NB!!!
                                    key = tx, ty , tz,  edges , extrapolate                              
                                    vh  = h3 ( x , y , z , interpolate= ( tx , ty , tz )  ,
                                               edges  = edges , extrapolate = extrapolate )                                     
                                    logger.info ( '3D: Key:%-30s x,y,z=(%.3f,%.3f,%.3f) h=%s' % ( key , x , y , z , vh.toString("(%.4f+-%.4f)") ) )  
                                    

# =============================================================================
if '__main__' == __name__ :

    test_1D1 () ## test interpolation for 1D-histograms
    test_2D1 () ## test interpolation for 2D-histograms
    test_3D1 () ## test interpolation for 3D-histograms

# =============================================================================
# The END 
# =============================================================================
