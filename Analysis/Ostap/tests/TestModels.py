#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
## @file TestModels.py
#
#  tests for various signal models 
#
#  @author Vanya BELYAEV Ivan.Belyaeve@itep.ru
#  @date 2014-05-11
# =============================================================================
"""
Tests for various background fit models  
"""
# =============================================================================
__version__ = "$Revision:"
__author__  = "Vanya BELYAEV Ivan.Belyaev@itep.ru"
__date__    = "2014-05-10"
__all__     = ()  ## nothing to be imported 
# =============================================================================
import ROOT, random
from   Ostap.PyRoUts import *
from   Ostap.Utils   import rooSilent 
# =============================================================================
# logging 
# =============================================================================
from AnalysisPython.Logger import getLogger
if '__main__' == __name__  or '__builtin__' == __name__ : 
    logger = getLogger ( 'Ostap.TestModels' )
else : 
    logger = getLogger ( __name__ )
# =============================================================================
logger.info ( 'Test for signal fit models from Analysis/Ostap')
# =============================================================================
## make simple test mass 
mass     = ROOT.RooRealVar ( 'test_mass' , 'Some test mass' , 3.0 , 3.2 )

## book very simple data set
varset0  = ROOT.RooArgSet  ( mass )
dataset0 = ROOT.RooDataSet ( dsID() , 'Test Data set-0' , varset0 )  

mmin, mmax = mass.minmax() 

## fill it 
m = VE(3.100,0.015**2)
for i in xrange(0,5000) :
    mass.value = m.gauss () 
    dataset0.add ( varset0    )

for i in xrange(0,500) :
    mass.value = random.uniform ( *mass.minmax() ) 
    dataset0.add ( varset0   )

print dataset0

import Ostap.FitModels as     Models 

models = []

# =============================================================================
## gauss PDF
# =============================================================================
logger.info ('Test Gauss_pdf' )
limits_sigma = m.error() , 0.5 *  m.error() , 1.5 * m.error()
signal_gauss = Models.Gauss_pdf ( name  = 'Gauss'      ,
                                  xvar  = mass         ,
                                  sigma = limits_sigma ) 

signal_gauss . mean  = m.value ()
signal_gauss . sigma = m.error () 


models.append ( signal_gauss )

# =============================================================================
## Gauss PDF
# =============================================================================
model_gauss = Models.Fit1D( signal     = signal_gauss ,
                            background = Models.Bkg_pdf ('BkgGauss', xvar = mass , power = 0 ) )
model_gauss.background.tau.fix(0)

with rooSilent() : 
    result, frame = model_gauss . fitTo ( dataset0 )
    result, frame = model_gauss . fitTo ( dataset0 )
    
    
if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d ' % ( result.status() , result.covQual()  ) )
    print result 
else :     
    print 'Signal & Background are: ', result ( 'S'         )[0] , result( 'B'           )[0]
    print 'Mean   & Sigma      are: ', result ( 'mean_Gauss')[0] , result( 'sigma_Gauss' )[0]


models.append ( model_gauss )


# =============================================================================
## CrystalBall PDF
# =============================================================================
logger.info ('Test CrystalBall_pdf' )
signal_gauss.sigma = 0.015 
model_cb = Models.Fit1D (
    signal = Models.CrystalBall_pdf ( name  = 'CB'  ,
                                      xvar  = mass  ,
                                      alpha = (1,2) , 
                                      sigma = signal_gauss.sigma ,  
                                      mean  = signal_gauss.mean  ) ,
    background = model_gauss.background  )

with rooSilent() : 
    result, frame = model_cb. fitTo ( dataset0 )
    model_cb.signal.alpha.release()
    result, frame = model_cb. fitTo ( dataset0 )
    result, frame = model_cb. fitTo ( dataset0 )

if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d ' % ( result.status() , result.covQual () ) )
    print result 
else : 
    print 'Signal & Background are: ', result ( 'S'         )[0] , result( 'B'           )[0]
    print 'Mean   & Sigma      are: ', result ( 'mean_Gauss')[0] , result( 'sigma_Gauss' )[0]
    print 'Alpha  & n          are: ', result ( model_cb.signal.alpha ) [ 0 ] , result ( model_cb.signal.n ) [ 0 ] 

models.append ( model_cb )

# =============================================================================
## right side CrystalBall PDF
# =============================================================================
logger.info ('Test CrystalBallRS_pdf' )
signal_gauss.sigma = 0.015 
model_cbrs = Models.Fit1D (
    signal = Models.CrystalBallRS_pdf ( name  = 'CBRS'    , 
                                        xvar  = mass      ,
                                        alpha = (1.5,2.1) ,
                                        sigma = signal_gauss.sigma ,  
                                        mean  = signal_gauss.mean  ) ,
    background = model_gauss.background  )

model_cbrs.signal.alpha.fix( 3) 
model_cbrs.signal.n    .fix(10) 

model_cbrs.S = 5000
model_cbrs.B =  500

with rooSilent() : 
    result, frame = model_cbrs. fitTo ( dataset0 )
    model_cbrs.signal.alpha.release()
    result, frame = model_cbrs. fitTo ( dataset0 )
    result, frame = model_cbrs. fitTo ( dataset0 )

if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d ' % ( result.status() , result.covQual () ) )
    print result 
else : 
    print 'Signal & Background are: ', result ( 'S'         )[0] , result( 'B'           )[0]
    print 'Mean   & Sigma      are: ', result ( 'mean_Gauss')[0] , result( 'sigma_Gauss' )[0]

models.append ( model_cbrs  )

# =============================================================================
## double sided CrystalBall PDF
# =============================================================================
logger.info ('Test CrystalBallDS_pdf' )
signal_gauss.sigma = 0.015 
model_cbds = Models.Fit1D (
    signal = Models.CB2_pdf ( name  = 'CB2' , 
                              xvar  = mass               ,
                              sigma = signal_gauss.sigma ,  
                              mean  = signal_gauss.mean  ) ,
    background = model_gauss.background  )

model_cbds.signal.aL.fix(2  ) 
model_cbds.signal.nL.fix(10 ) 
model_cbds.signal.aR.fix(2.5) 
model_cbds.signal.nR.fix(10 ) 

model_cbds.S = 5000
model_cbds.B =  500

with rooSilent() : 
    result, frame = model_cbds. fitTo ( dataset0 )
    model_cbds.signal.aL.release()
    model_cbds.signal.aR.release()
    result, frame = model_cbds. fitTo ( dataset0 )
    model_cbds.signal.aL.fix(5) 
    model_cbds.signal.aL.fix(5)    
    result, frame = model_cbds. fitTo ( dataset0 )

if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d ' % ( result.status() , result.covQual () ) )
    print result 
else : 
    print 'Signal & Background are: ', result ( 'S'         )[0] , result( 'B'           )[0]
    print 'Mean   & Sigma      are: ', result ( 'mean_Gauss')[0] , result( 'sigma_Gauss' )[0]

models.append ( model_cbds  )


# =============================================================================
## Needham PDF
# =============================================================================
logger.info ('Test Needham_pdf' )
signal_gauss.sigma = 0.015 
model_matt = Models.Fit1D (
    signal = Models.Needham_pdf ( name  = 'Matt' , 
                                  xvar  = mass               ,
                                  sigma = signal_gauss.sigma ,  
                                  mean  = signal_gauss.mean  ) ,
    background = model_gauss.background  )

with rooSilent() : 
    result, frame = model_matt. fitTo ( dataset0 )
    model_matt.signal.mean .release()
    model_matt.signal.sigma.release()
    result, frame = model_matt. fitTo ( dataset0 )
    result, frame = model_matt. fitTo ( dataset0 )

if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d ' % ( result.status() , result.covQual () ) )
    print result 
else : 
    print 'Signal & Background are: ', result ( 'S'         )[0] , result( 'B'           )[0]
    print 'Mean   & Sigma      are: ', result ( 'mean_Gauss')[0] , result( 'sigma_Gauss' )[0]


models.append ( model_matt  )

# =============================================================================
## DoubleGauss PDF
# =============================================================================
logger.info ('Test DoubleGauss_pdf' )
model_2g = Models.Fit1D(
    signal = Models.DoubleGauss_pdf ( 'Gauss2' ,
                                      xvar     = mass  ,
                                      sigma    = signal_gauss.sigma ,
                                      scale    = (1.2,2.0) , 
                                      fraction = (0.1,0.9) ) , background = 1 )
model_2g.background.tau.fix(0)
s2g = model_2g.signal
s2g.sigma.fix( 0.8 * m.error() ) 
with rooSilent() : 
    result, frame = model_2g . fitTo ( dataset0 )
    result, frame = model_2g . fitTo ( dataset0 )
    
    
if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d ' % ( result.status() , result.covQual()  ) )
    print result 
else :     
    print 'Signal & Background are: ', result ( 'S'         )[0] , result( 'B'           )[0]
    print 'Mean   & Sigma      are: ', result ( s2g.mean    )[0] , result( s2g.sigma     )[0]
    print 'Scale  & Fraction   are: ', result ( s2g.scale   )[0] , result( s2g.fraction  )[0]


models.append ( model_2g )
# ==========================================================================
## Apolonios
# ==========================================================================
logger.info ('Test Apolonios_pdf' ) 
signal_gauss.sigma = 0.015 
model_apolonios = Models.Fit1D (
    signal = Models.Apolonios_pdf ( name  = 'APO', 
                                    xvar  = mass ,
                                    mean  = signal_gauss.mean    ,
                                    sigma = signal_gauss.sigma ,
                                    b     = (0.5,2) ,
                                    n     = 10 ,
                                    alpha =  3 ) , background = 1 )

model_apolonios.S = 5000
model_apolonios.B =  500

with rooSilent() : 
    result, frame = model_apolonios. fitTo ( dataset0 )
    result, frame = model_apolonios. fitTo ( dataset0 )
    
if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d ' % ( result.status() , result.covQual () ) )
    print result 
else : 
    print 'Signal & Background are: ', result ( 'S'         )[0] , result( 'B'           )[0]
    print 'Mean   & Sigma      are: ', result ( 'mean_Gauss')[0] , result( 'sigma_Gauss' )[0]

models.append ( model_apolonios )

# ==========================================================================
## Apolonios2
# ==========================================================================
logger.info ('Test Apolonios2_pdf' ) 
signal_gauss.sigma = 0.015 
model_apolonios2   = Models.Fit1D (
    signal = Models.Apolonios2_pdf ( name = 'AP2' , 
                                     xvar      = mass ,
                                     mean      = signal_gauss.mean  ,
                                     sigma     = signal_gauss.sigma ,
                                     asymmetry = 0           ,
                                     beta      = ( 0.5, 2) ) ,
    background = model_gauss.background  )

model_apolonios2.S = 5000
model_apolonios2.B =  500

with rooSilent() : 
    result, frame = model_apolonios2. fitTo ( dataset0 )
    model_apolonios2.signal.asym.release () 
    model_apolonios2.B =  500
    result, frame = model_apolonios2. fitTo ( dataset0 )
    
if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d ' % ( result.status() , result.covQual () ) )
    print result 
else : 
    print 'Signal & Background are: ', result ( 'S'         )[0] , result( 'B'           )[0]
    print 'Mean   & Sigma      are: ', result ( 'mean_Gauss')[0] , result( 'sigma_Gauss' )[0]

models.append ( model_apolonios2 )



# =============================================================================
## Bifurcated gauss PDF
# =============================================================================
logger.info ('Test BifurcatedGauss_pdf' ) 
signal_gauss.sigma = 0.015 
signal_bifurcated  = Models.BifurcatedGauss_pdf ( name      = 'BfGau' ,
                                                  mean      = signal_gauss.mean  ,
                                                  sigma     = signal_gauss.sigma ,
                                                  xvar      = mass    ,
                                                  asymmetry = 0       ,
                                                  )


model_bifurcated = Models.Fit1D( signal     = signal_bifurcated       ,
                                 background = model_gauss.background  )

model_bifurcated.S = 5000 
model_bifurcated.B =  500 
signal_gauss.mean  .fix ( m.value() )
signal_gauss.sigma .fix ( m.error() )

with rooSilent() : 
    result, frame = model_bifurcated . fitTo ( dataset0 )
    signal_bifurcated . asym.release () 
    signal_gauss.mean . release() 
    result, frame = model_bifurcated . fitTo ( dataset0 )
    signal_gauss.sigma. release()    
    result, frame = model_bifurcated . fitTo ( dataset0 )

if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d ' % ( result.status() , result.covQual () ) )
    print result 
else :     
    print 'Signal & Background are: ', result ( 'S'         )[0] , result( 'B'           )[0]
    print 'Mean   & Sigma      are: ', result ( 'mean_Gauss')[0] , result( 'sigma_Gauss' )[0]

signal_gauss.mean  .release() 
signal_gauss.sigma .release() 
models.append ( model_bifurcated  )


# =============================================================================
## GenGaussV1
# ============================================================================= 
logger.info ('Test GenGaussV1_pdf' ) 
signal_gauss.mean   = m.value() 
signal_gauss.sigma  = m.error() 
model_gauss_gv1 = Models.Fit1D (
    signal = Models.GenGaussV1_pdf ( name = 'Gv1' , 
                                     xvar = mass  ,
                                     mean = signal_gauss.mean ) ,
    background = model_gauss.background  )

model_gauss_gv1.signal.beta .fix(2)
model_gauss_gv1.signal.mean .fix( m.value() )

model_gauss_gv1.S = 5000
model_gauss_gv1.B =  500

with rooSilent() : 
    result, frame = model_gauss_gv1. fitTo ( dataset0 )
    model_gauss_gv1.signal.alpha.release()
    result, frame = model_gauss_gv1. fitTo ( dataset0 )
    model_gauss_gv1.signal.mean .release() 
    result, frame = model_gauss_gv1. fitTo ( dataset0 )

if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d ' % ( result.status() , result.covQual () ) )
    print result 
else :     
    print 'Signal & Background are: ', result ( 'S'         )[0] , result( 'B'           )[0]
    print 'Mean                is: ' , result ( 'mean_Gauss')[0] 

models.append ( model_gauss_gv1  )

# =============================================================================
## GenGaussV2
# =============================================================================
logger.info ('Test GenGaussV2_pdf' ) 
model_gauss_gv2 = Models.Fit1D (
    signal = Models.GenGaussV2_pdf ( name = 'Gv2' , 
                                     xvar = mass  ,
                                     mean = signal_gauss.mean ) ,
    background = model_gauss.background  )

model_gauss_gv2.signal.kappa.fix(0)

model_gauss_gv2.S = 5000
model_gauss_gv2.B =  500

with rooSilent() : 
    result, frame = model_gauss_gv2. fitTo ( dataset0 )
    model_gauss_gv2.signal.mean.release()    
    result, frame = model_gauss_gv2. fitTo ( dataset0 )
    ##model_gauss_gv2.signal.kappa.release() 
    result, frame = model_gauss_gv2. fitTo ( dataset0 )
    result, frame = model_gauss_gv2. fitTo ( dataset0 )
    result, frame = model_gauss_gv2. fitTo ( dataset0 )
    
if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d ' % ( result.status() , result.covQual () ) )
    print result 
else :
    print 'Signal & Background are: ', result ( 'S'         )[0] , result( 'B'           )[0]
    print 'Mean                is: ' , result ( 'mean_Gauss')[0] 

models.append ( model_gauss_gv2  )

# =============================================================================
## SkewGauss
# =============================================================================
logger.info ('Test SkewGauss_pdf' ) 
signal_gauss.sigma = 0.015 
model_gauss_skew = Models.Fit1D (
    signal = Models.SkewGauss_pdf ( name = 'GSk' , 
                                    xvar = mass  , mean = signal_gauss.mean ) ,
    background = model_gauss.background  )

model_gauss_skew.signal.alpha.fix(0)

model_gauss_skew.S = 5000 
model_gauss_skew.B =  500 

with rooSilent() : 
    result, frame = model_gauss_skew. fitTo ( dataset0 )
    result, frame = model_gauss_skew. fitTo ( dataset0 )
    
if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d ' % ( result.status() , result.covQual () ) )
    print result 
else : 
    print 'Signal & Background are: ', result ( 'S'         )[0] , result( 'B'           )[0]
    print 'Mean                is: ' , result ( 'mean_Gauss')[0] 

models.append ( model_gauss_skew  )

# =============================================================================
## Bukin
# =============================================================================
logger.info ('Test Bukin_pdf' ) 
signal_gauss.sigma = 0.015 
model_bukin = Models.Fit1D (
    signal = Models.Bukin_pdf ( name  = 'Bukin' ,
                                xvar  = mass    ,
                                xi    = 0    ,
                                rhoL  = 0    ,
                                rhoR  = 0    , 
                                mean  = signal_gauss.mean  , 
                                sigma = signal_gauss.sigma ) ,
    background = model_gauss.background  )


model_bukin.signal.mean .fix  ( m.value() )
model_bukin.signal.sigma.fix  ( m.error() )

model_bukin.S = 5000 
model_bukin.B =  500 

with rooSilent() : 
    result, frame = model_bukin. fitTo ( dataset0 )
    model_bukin.signal.xi  .release()     
    result, frame = model_bukin. fitTo ( dataset0 )
    model_bukin.signal.rhoL.release()     
    result, frame = model_bukin. fitTo ( dataset0 )
    model_bukin.signal.rhoR.release()     
    result, frame = model_bukin. fitTo ( dataset0 )
    model_bukin.signal.mean .release() 
    model_bukin.signal.sigma.release() 
    result, frame = model_bukin. fitTo ( dataset0 )
    
if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d ' % ( result.status() , result.covQual () ) )
    print result 
else : 
    print 'Signal & Background are: ', result ( 'S'         )[0] , result( 'B'           )[0]
    print 'Mean                is: ' , result ( 'mean_Gauss')[0] 


models.append ( model_bukin  )

# =============================================================================
## StudentT
# =============================================================================
logger.info ('Test StudentT_pdf' ) 
signal_gauss.sigma = 0.015 
model_student = Models.Fit1D (
    signal = Models.StudentT_pdf ( name = 'ST' , 
                                   xvar = mass ,
                                   mean = signal_gauss.mean ) ,
    background = model_gauss.background  )


model_student.signal.N     = 10
model_student.signal.sigma = 0.013

model_student.S = 5000 
model_student.B =  500 

with rooSilent() : 
    result, frame = model_student. fitTo ( dataset0 )
    result, frame = model_student. fitTo ( dataset0 )
    
if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d ' % ( result.status() , result.covQual () ) )
    print result 
else : 
    print 'Signal & Background are: ', result ( 'S'         )[0] , result( 'B'           )[0]
    print 'Mean                is: ' , result ( 'mean_Gauss')[0] 

models.append ( model_student  )

# =============================================================================
## Bifurcated StudentT
# =============================================================================
logger.info ('Test bifurcated StudentT_pdf' ) 
signal_gauss.sigma = 0.015 
model_bstudent = Models.Fit1D (
    signal = Models.BifurcatedStudentT_pdf ( name  = 'BfST' , 
                                             xvar  = mass ,
                                             mean  = signal_gauss.mean  ,
                                             sigma = signal_gauss.sigma ) ,
    background = model_gauss.background  )


model_bstudent.signal.nL.fix(25)
model_bstudent.signal.nR.fix(25)

model_bstudent.S = 5000 
model_bstudent.B =  500 

with rooSilent() : 
    result, frame = model_bstudent. fitTo ( dataset0 )
    result, frame = model_bstudent. fitTo ( dataset0 )
    
if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d ' % ( result.status() , result.covQual () ) )
    print result 
else : 
    print 'Signal & Background are: ', result ( 'S' )[0] , result( 'B' )[0]
    print 'Mean                is: ' , result ( model_bstudent.signal.mean  .GetName() )[0] 
    print 'Sigma               is: ' , result ( model_bstudent.signal.sigma .GetName() )[0] 
    print 'Asymmetry           is: ' , result ( model_bstudent.signal.asym  .GetName() )[0] 
    print 'n(L)                is: ' , result ( model_bstudent.signal.nL    .GetName() )[0] 
    print 'n(R)                is: ' , result ( model_bstudent.signal.nR    .GetName() )[0] 
    
models.append ( model_bstudent  )

# =============================================================================
logger.info("Test  SinhAsinh-Distribution")
# =============================================================================
model_shash = Models.Fit1D (
    signal = Models.SinhAsinh_pdf( 'SASH'                     ,
                                   xvar  = mass               , 
                                   mean  = signal_gauss.mean  ,
                                   sigma = signal_gauss.sigma ) ,
    background = model_gauss.background  )


model_shash.S = 5000
model_shash.B =  500

signal = model_shash.signal
signal.mu      = 3.10  
signal.sigma   = 0.015 
signal.epsilon = 0.021 
signal.delta   = 1.0   

with rooSilent() : 
    result,f  = model_shash.fitTo ( dataset0 )  
    result,f  = model_shash.fitTo ( dataset0 )  
    signal.delta.release()
    result,f  = model_shash.fitTo ( dataset0 )  
    signal.epsilon.release()
    result,f  = model_shash.fitTo ( dataset0 )  
        
if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d ' % ( result.status() , result.covQual()  ) )
    print result
else :
    print  "\tSinhAsinh:   mu   = %s " % result( signal.mu      )[0]   
    print  "\tSinhAsinh:   sigma= %s " % result( signal.sigma   )[0]   
    print  "\tSinhAsinh:   eps  = %s " % result( signal.epsilon )[0]   
    print  "\tSinhAsinh:   delta= %s " % result( signal.delta   )[0]   

models.append ( model_shash )

# =============================================================================
logger.info("Test  JohnsonSU-Distribution")
# =============================================================================
model_jsu = Models.Fit1D (
    signal = Models.JohnsonSU_pdf( 'JSU'                    ,
                                   xvar = mass              , 
                                   xi   = signal_gauss.mean ) ,
    background = model_gauss.background  )

signal = model_jsu.signal

model_jsu.S = 5000
model_jsu.B =  500

with rooSilent() : 
    result,f  = model_jsu.fitTo ( dataset0 )  
    result,f  = model_jsu.fitTo ( dataset0 )  
    model_jsu.signal.lambd.release()
    result,f  = model_jsu.fitTo ( dataset0 )  
    model_jsu.signal.delta.release()
    result,f  = model_jsu.fitTo ( dataset0 )  
    
if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d ' % (
        result.status() , result.covQual()  ) )
    print result
else :
    print  "\tJohnsonSU:   xi   = %s " % result ( signal.xi      )[0]
    print  "\tJohnsonSU:   lam  = %s " % result ( signal.lambd   )[0]
    print  "\tJohnsonSU:   delta= %s " % result ( signal.delta   )[0]
    print  "\tJohnsonSU:   gamma= %s " % result ( signal.gamma   )[0]

models.append ( model_jsu )

# =============================================================================
logger.info("Test  ATLAS")
# =============================================================================
model_atlas = Models.Fit1D (
    signal = Models.Atlas_pdf( 'ATLAS'                  ,
                               xvar = mass              , 
                               mean = signal_gauss.mean ) ,
    background = model_gauss.background  )

signal = model_atlas.signal

model_atlas.S = 5000
model_atlas.B =  500

with rooSilent() : 
    result,f  = model_atlas.fitTo ( dataset0 )  
    result,f  = model_atlas.fitTo ( dataset0 )  
    model_atlas.signal.mean  .release()
    model_atlas.signal.sigma .release()
    result,f  = model_atlas.fitTo ( dataset0 )  
    
if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d ' % (
        result.status() , result.covQual()  ) )
    print result
else :
    print  "\tAtlas:       mean = %s " % result ( signal.mean    )[0]
    print  "\tAtlas:       sigma= %s " % result ( signal.sigma   )[0]

models.append ( model_atlas )

# =============================================================================
logger.info("Test  SECH")
# =============================================================================
signal_gauss.sigma = 0.015 
model_sech = Models.Fit1D (
    signal = Models.Sech_pdf( 'SECH'                    ,
                              xvar = mass              , 
                              mean = signal_gauss.mean ) ,
    background = model_gauss.background  )

signal = model_sech.signal

model_sech.S = 5000
model_sech.B =  500

with rooSilent() : 
    result,f  = model_sech.fitTo ( dataset0 )  
    result,f  = model_sech.fitTo ( dataset0 )  
    model_sech.signal.mean  .release()
    model_sech.signal.sigma .release()
    result,f  = model_sech.fitTo ( dataset0 )  
    
if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d ' % (
        result.status() , result.covQual()  ) )
    print result
else :
    print  "\tSech :       mean = %s " % result ( signal.mean    )[0]
    print  "\tSech :       sigma= %s " % result ( signal.sigma   )[0]

models.append ( model_sech )

# =============================================================================
logger.info("Test  LOGICTIC")
# =============================================================================
model_logi = Models.Fit1D (
    signal = Models.Logistic_pdf( 'LOGI'                    ,
                                  xvar = mass              , 
                                  mean = signal_gauss.mean ) ,
    background = model_gauss.background  )

signal = model_logi.signal
model_logi.S = 5000
model_logi.B =  500

with rooSilent() : 
    result,f  = model_logi.fitTo ( dataset0 )  
    result,f  = model_logi.fitTo ( dataset0 )  
    model_logi.signal.mean  .release()
    model_logi.signal.sigma .release()
    result,f  = model_logi.fitTo ( dataset0 )  
    
if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d ' % (
        result.status() , result.covQual()  ) )
    print result
else :
    print  "\tSech :       mean = %s " % result ( signal.mean    )[0]
    print  "\tSech :       sigma= %s " % result ( signal.sigma   )[0]

models.append ( model_logi )

# =============================================================================
## Voigt
# =============================================================================
logger.info ('Test Voigt_pdf' )
signal_gauss.sigma =  0.015
model_vgt = Models.Fit1D (
    signal = Models.Voigt_pdf ( 'V' , 
                                xvar  = mass                ,
                                mean  = signal_gauss.mean   , 
                                sigma = signal_gauss.sigma  ) , 
    background = model_gauss.background  )

signal = model_vgt.signal
signal.sigma.fix ( m.error() )
signal.gamma.fix ( 0.010     )

model_vgt.S = 5000
model_vgt.B =  500

with rooSilent() : 
    result, frame = model_vgt. fitTo ( dataset0 )
    model_vgt.signal.gamma.release() 
    result, frame = model_vgt. fitTo ( dataset0 )
    model_vgt.signal.sigma.release() 
    result, frame = model_vgt. fitTo ( dataset0 )
    
if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d ' % ( result.status() , result.covQual () ) )
    print result 
else :
    print 'Signal & Background are: ', result ( 'S'           )[0] , result ( 'B'           )[0]
    print 'Mean   & Gamma      are: ', result ( signal.mean   )[0] , result ( signal.gamma  )[0]
    print 'Sigma               is : ', result ( signal.sigma  )[0] 
    
models.append ( model_vgt )


# =============================================================================
## PseudoVoigt
# =============================================================================
signal_gauss.sigma =  0.015
logger.info ('Test PSeudoVoigt_pdf' )
model_pvgt = Models.Fit1D (
    signal = Models.PseudoVoigt_pdf ( '{V' , 
                                      xvar  = mass                ,
                                      mean  = signal_gauss.mean   , 
                                      sigma = signal_gauss.sigma  ) , 
    background = model_gauss.background  )

signal = model_pvgt.signal
signal.sigma.fix ( m.error() )
signal.gamma.fix ( 0.010     )

model_pvgt.S = 5000
model_pvgt.B =  500

with rooSilent() : 
    result, frame = model_pvgt. fitTo ( dataset0 )
    model_pvgt.signal.gamma.release() 
    result, frame = model_pvgt. fitTo ( dataset0 )
    model_pvgt.signal.sigma.release() 
    result, frame = model_pvgt. fitTo ( dataset0 )
    
if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d ' % ( result.status() , result.covQual () ) )
    print result 
else :
    print 'Signal & Background are: ', result ( 'S'           )[0] , result ( 'B'           )[0]
    print 'Mean   & Gamma      are: ', result ( signal.mean   )[0] , result ( signal.gamma  )[0]
    print 'Sigma               is : ', result ( signal.sigma  )[0] 
    
models.append ( model_pvgt )



# =============================================================================
## Breit-Wigner
# =============================================================================
logger.info ('Test BreitWigner_pdf ' )
ff = cpp.Gaudi.Math.FormFactors.BlattWeisskopf( 1 , 3.5 ) ## formfactor 
bw = cpp.Gaudi.Math.BreitWigner (
    m.value() ,
    m.error() ,
    0.150     ,
    0.150     ,
    1         , ## orbital momentum
    ff          ## formfactor 
    )

""" 
model_bw0 = Models.Fit1D (
    signal = Models.BreitWigner_pdf (
    name        = 'BW0'             ,
    breitwigner = bw                ,     
    xvar        = mass              ,
    mean        = signal_gauss.mean ,
    gamma       = ( m.error() , m.error()/5, 2*m.error() ) ) ,
    background = model_gauss.background  )

model_bw0.S = 5000
model_bw0.B.fix(500) 

signal_bw0 = model_bw0.signal
signal_bw0.mean.fix ( m.value() )

with rooSilent() : 
    result, frame = model_bw0. fitTo ( dataset0 )
    signal_bw0.mean .release()
    signal_bw0.gamma.release()
    result, frame = model_bw0. fitTo ( dataset0 )
    
if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d ' % ( result.status() , result.covQual () ) )
    print result 
else :
    print 'Signal & Background are: ', result (  model_bw0.S    )[0] , result(  model_bw0.B     )[0]
    print 'Mean   & Gamma      are: ', result ( signal_bw0.mean )[0] , result( signal_bw0.gamma )[0]

models.append ( model_bw0)
"""


#
## add simple Gaussian convolution 
#
logger.info ('Test BreitWigner_pdf with 10MeV gaussian resolution' )
model_bw1 = Models.Fit1D (
    signal = Models.BreitWigner_pdf (
    name        = 'BW1'             ,
    breitwigner = bw                ,     
    xvar        = mass              ,
    mean        = signal_gauss.mean ,
    gamma       = ( m.error() , m.error()/5, 2*m.error() ) , 
    convolution = 0.010 ) ,
    background = model_gauss.background  )

model_bw1.S = 5000
model_bw1.B =  500

signal_bw1 = model_bw1.signal
signal_bw1.mean.fix ( m.value() )

with rooSilent() : 
    result, frame = model_bw1. fitTo ( dataset0 )
    signal_bw1.mean .release()
    signal_bw1.gamma.release()
    result, frame = model_bw1. fitTo ( dataset0 )
    
if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d ' % ( result.status() , result.covQual () ) )
    print result 
else :
    print 'Signal & Background are: ', result (  model_bw1.S    )[0] , result(  model_bw1.B     )[0]
    print 'Mean   & Gamma      are: ', result ( signal_bw1.mean )[0] , result( signal_bw1.gamma )[0]

models.append ( model_bw1 )


# ==============================================================================
## add  Gaussian PDF 
# ==============================================================================
logger.info ('Test BreitWigner_pdf with (5-20MeV) gaussian resolution' )
from Ostap.FitResolution import ResoGauss
reso_gauss2 = ResoGauss('GR2',  xvar  = mass ,  sigma = (0.010,0.005,0.020) )


model_bw2 = Models.Fit1D (
    signal = Models.BreitWigner_pdf (
    name        = 'BW2'             ,
    breitwigner = bw                ,     
    xvar        = mass              ,
    mean        = signal_gauss.mean ,
    gamma       = ( m.error() , m.error()/5, 2*m.error() ) , 
    convolution = reso_gauss2 ) ,
    background = model_gauss.background  )

model_bw2.S = 5000
model_bw2.B =  500

signal_bw2 = model_bw2.signal
signal_bw2.mean.fix ( m.value() )

with rooSilent() : 
    result, frame = model_bw2. fitTo ( dataset0 )
    signal_bw2.mean .release()
    signal_bw2.gamma.release()
    result, frame = model_bw2. fitTo ( dataset0 )
    
if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d ' % ( result.status() , result.covQual () ) )
    print result 
else :
    print 'Signal & Background are: ', result (  model_bw2.S     )[0] , result(  model_bw2.B     )[0]
    print 'Mean   & Gamma      are: ', result ( signal_bw2.mean  )[0] , result( signal_bw2.gamma )[0]
    print 'Sigma  is              : ', result ( reso_gauss2.sigma )[0] 

models.append ( model_bw2 )



# ==============================================================================
## add  Gaussian PDF 
# ==============================================================================
logger.info ('Test BreitWigner_pdf with double gaussian resolution' )
from Ostap.FitResolution import ResoGauss2
reso_2g = ResoGauss2('GR2',  xvar  = mass ,  sigma = (0.010,0.005,0.020) )


model_bw3 = Models.Fit1D (
    signal = Models.BreitWigner_pdf (
    name        = 'BW2'             ,
    breitwigner = bw                ,     
    xvar        = mass              ,
    mean        = signal_gauss.mean ,
    gamma       = ( m.error() , m.error()/5, 2*m.error() ) , 
    convolution = reso_2g ) ,
    background = model_gauss.background  )

model_bw3.S = 5000
model_bw3.B =  500

signal_bw3 = model_bw3.signal
signal_bw3.mean.fix ( m.value() )

with rooSilent() : 
    result, frame = model_bw3. fitTo ( dataset0 )
    signal_bw3.mean .release()
    signal_bw3.gamma.release()
    result, frame = model_bw3. fitTo ( dataset0 )
    
if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d ' % ( result.status() , result.covQual () ) )
    print result 
else :
    print 'Signal & Background are: ', result (  model_bw3.S     )[0] , result(  model_bw3.B     )[0]
    print 'Mean   & Gamma      are: ', result ( signal_bw3.mean  )[0] , result( signal_bw3.gamma )[0]
    print 'Sigma  is              : ', result ( reso_2g.sigma )[0] 

models.append ( model_bw3 )


"""
#
## check that everything is serializable
#
logger.info('Saving all objects into DBASE')
import Ostap.ZipShelve   as     DBASE
from   Ostap.Utils       import timing
with timing(), DBASE.tmpdb() as db : 
    db['mass,vars'] = mass, varset0
    db['dataset'  ] = dataset0
    db['models'   ] = models
    db['result'   ] = result
    db['frame'    ] = frame
    db.ls() 
"""

# =============================================================================
# The END 
# =============================================================================
