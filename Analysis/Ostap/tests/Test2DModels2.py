#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
## @file Test2DModels2.py
#
#  tests for various 2D-fit modelsa
#
#  @author Vanya BELYAEV Ivan.Belyaeve@itep.ru
#  @date 2014-05-11
# =============================================================================
"""Tests for various 2D-fit modelsa
"""
# =============================================================================
__version__ = "$Revision:"
__author__  = "Vanya BELYAEV Ivan.Belyaev@itep.ru"
__date__    = "2014-05-10"
__all__     = ()  ## nothing to be imported 
# =============================================================================
import ROOT, random 
from   Ostap.PyRoUts import *
from   Ostap.Utils   import rooSilent 
# =============================================================================
# logging 
# =============================================================================
from AnalysisPython.Logger import getLogger
if '__main__' == __name__ or '__builtin__' == __name__ : 
    logger = getLogger( 'Ostap.Test2DModels2' )
else : 
    logger = getLogger( ___ )
# =============================================================================
logger.info ( 'Test for 2D nonfactorizable fit models from Analysis/Ostap')
# =============================================================================
## make simple test mass 
m_x     = ROOT.RooRealVar ( 'mass_x' , 'Some test mass(X)' , 3 , 3.2 )
m_y     = ROOT.RooRealVar ( 'mass_y' , 'Some test mass(Y)' , 3 , 3.2 )

## book very simple data set
varset  = ROOT.RooArgSet  ( m_x , m_y )
dataset = ROOT.RooDataSet ( dsID() , 'Test Data set-1' , varset )  

m = VE(3.100,0.015**2)

N_ss =  5000
N_sb =  2500
N_bs =  2500
N_bb =  5000

random.seed(0)

## fill it : 5000 events  Gauss * Gauss 
for i in xrange(0,N_ss) : 
    m_x.value = m.gauss() 
    m_y.value = m.gauss() 
    dataset.add ( varset  )

## fill it : 2500 events  Gauss * const  
for i in xrange(0,N_sb) : 
    m_x.value = m.gauss() 
    m_y.value = random.uniform ( *m_y.minmax() )
    dataset.add ( varset  )

## fill it : 2500 events  const * Gauss
for i in xrange(0,N_bs) : 
    m_x.setVal  ( random.uniform ( m_x.getMin() , m_x.getMax() ) ) 
    m_y.setVal  ( m.gauss() )
    dataset.add ( varset  )

## fill it : 5000 events  const * const
for i in xrange(0,N_bb) :

    m_x.setVal ( random.uniform ( m_x.getMin() , m_x.getMax() ) ) 
    m_y.setVal ( random.uniform ( m_y.getMin() , m_y.getMax() ) ) 
    dataset.add ( varset  )


print dataset
# =============================================================================


import Ostap.FitModels as     Models 
models = [] 

signal1  = Models.Gauss_pdf ( 'Gx'    , xvar = m_x )
## independent signal 
signal2  = Models.Gauss_pdf ( 'Gy'    , xvar = m_y )
## symmetrized signal 
signal2s = Models.Gauss_pdf ( 'GySym' , xvar = m_y  ,
                              mean  = signal1.mean  ,
                              sigma = signal1.sigma )
                                 

# =============================================================================
## gauss as signal, const as background 
# =============================================================================
logger.info ('Simplest (factorized) fit model:  ( Gauss + const ) x ( Gauss + const ) ' )
model   = Models.Fit2D (
    signal_1 = signal1 ,
    signal_2 = signal2 
    )

model.signal1.sigma.fix ( m.error () )
model.signal2.sigma.fix ( m.error () )
model.signal1.mean .fix ( m.value () )
model.signal2.mean .fix ( m.value () )
model.signal1.mean .fix ( m.value () )
model.signal2.mean .fix ( m.value () )

## fit with fixed mass and sigma
with rooSilent() : 
    result, frame = model. fitTo ( dataset )
    model.signal1.sigma.release () 
    model.signal2.sigma.release ()
    model.signal1.mean .release () 
    model.signal2.mean .release () 
    result, frame = model. fitTo ( dataset )


if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d '
                   % ( result.status() , result.covQual()  ) )
    print result 

print 'SxS=%s \nSxB=%s \nBxS=%s \nBxB=%s ' % ( result ( model.SS ) [0] ,
                                               result ( model.SB ) [0] ,
                                               result ( model.BS ) [0] ,
                                               result ( model.BB ) [0] ) 

model_1 = model 
models.append ( model ) 
# =============================================================================
## gauss as signal, second order polynomial as background 
# =============================================================================
logger.info ('Simple (factorized) fit model:  ( Gauss + P2 ) (x) ( Gauss + P2 ) ' )
model   = Models.Fit2D (
    suffix   = '_2' , 
    signal_1 = signal1 ,  
    signal_2 = signal2 , 
    bkg1   = 2 , 
    bkg2   = 2 ,
    bkgA   = 2 ,
    bkgB   = 2 , 
    )

model.signal1.sigma.fix ( m.error () )
model.signal2.sigma.fix ( m.error () )
model.signal1.mean .fix ( m.value () )
model.signal2.mean .fix ( m.value () )
model.signal1.mean .fix ( m.value () )
model.signal2.mean .fix ( m.value () )
model.bkg1   .tau  .fix ( 0 )
model.bkg2   .tau  .fix ( 0 )
model.bkgA   .tau  .fix ( 0 )
model.bkgB   .tau  .fix ( 0 )

## fit with fixed mass and sigma
with rooSilent() : 
    result, frame = model. fitTo ( dataset )
    # model.signal1.sigma.release () 
    # model.signal2.sigma.release ()
    # model.signal1.mean .release () 
    # model.signal2.mean .release () 
    result, frame = model. fitTo ( dataset )


if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d '
                   % ( result.status() , result.covQual()  ) )
    print result 

print 'SxS=%s \nSxB=%s \nBxS=%s \nBxB=%s ' % ( result ( model.SS ) [0] ,
                                               result ( model.SB ) [0] ,
                                               result ( model.BS ) [0] ,
                                               result ( model.BB ) [0] ) 


model_2 = model 
models.append ( model ) 
# =============================================================================
## gauss as signal, 1st order polynomial as background 
# =============================================================================
logger.info ('Simplest non-factorized fit model:  ( Gauss + P1 ) (x) ( Gauss + P1 ) + BB' )
model   = Models.Fit2D (
    suffix   = '_3' , 
    signal_1 = signal1 , 
    signal_2 = signal2 , 
    bkg1     = 1 , 
    bkg2     = 1 ,
    bkg2D    = Models.PolyPos2D_pdf ( 'P2D' , m_x , m_y , nx = 2 , ny = 2 ) 
    )

model.signal1.sigma.fix ( m.error () )
model.signal2.sigma.fix ( m.error () )
model.signal1.mean .fix ( m.value () )
model.signal2.mean .fix ( m.value () )
model.signal1.mean .fix ( m.value () )
model.signal2.mean .fix ( m.value () )
model.bkg1   .tau  .fix ( 0 )
model.bkg2   .tau  .fix ( 0 )

## fit with fixed mass and sigma
with rooSilent() : 
    result, frame = model. fitTo ( dataset )
    # model.signal1.sigma.release () 
    # model.signal2.sigma.release ()
    # model.signal1.mean .release () 
    # model.signal2.mean .release () 
    result, frame = model. fitTo ( dataset )
    
    
if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d '
                   % ( result.status() , result.covQual()  ) )
    print result 

print 'SxS=%s \nSxB=%s \nBxS=%s \nBxB=%s ' % ( result ( model.SS ) [0] ,
                                               result ( model.SB ) [0] ,
                                               result ( model.BS ) [0] ,
                                               result ( model.BB ) [0] ) 


model_3 = model 
models.append ( model ) 
# =============================================================================
## gauss as signal, 1st order polynomial as background 
# =============================================================================
logger.info ('Non-factorized symmetric background fit model:  ( Gauss + P1 ) (x) ( Gauss + P1 ) + BBsym' )
model   = Models.Fit2D (
    suffix   = '_4' , 
    signal_1 = signal1  , 
    signal_2 = signal2s , 
    bkg1     = 1 , 
    bkg2     = 1 ,
    bkg2D    = Models.PolyPos2Dsym_pdf ( 'P2Ds' , m_x , m_y , n = 2 ) 
    )

model.signal1.sigma.fix ( m.error () )
model.signal2.sigma.fix ( m.error () )
model.signal1.mean .fix ( m.value () )
model.signal2.mean .fix ( m.value () )
model.signal1.mean .fix ( m.value () )
model.signal2.mean .fix ( m.value () )
model.bkg1   .tau  .fix ( 0 )
model.bkg2   .tau  .fix ( 0 )

## fit with fixed mass and sigma
with rooSilent() : 
    result, frame = model. fitTo ( dataset )
    # model.signal1.sigma.release () 
    # model.signal2.sigma.release ()
    # model.signal1.mean .release () 
    # model.signal2.mean .release () 
    result, frame = model. fitTo ( dataset )
    

if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d '
                   % ( result.status() , result.covQual()  ) )
    print result 

print 'SxS=%s \nSxB=%s \nBxS=%s \nBxB=%s ' % ( result ( model.SS ) [0] ,
                                               result ( model.SB ) [0] ,
                                               result ( model.BS ) [0] ,
                                               result ( model.BB ) [0] ) 


model_4 = model 
models.append ( model ) 

# =============================================================================
## gauss as signal, 1st order polynomial as background 
# =============================================================================
logger.info ('Symmetrised fit model with non-factorized symmetric background:  ( Gauss + P1 ) (x) ( Gauss + P1 ) + BBsym' )
sb      = ROOT.RooRealVar('sb','SB',0,10000)
model   = Models.Fit2D (
    suffix   = '_5' , 
    signal_1 = signal1  , 
    signal_2 = signal2s , 
    bkg1     = 1  , 
    bkg2     = 1  ,
    bkg2D    = Models.PolyPos2Dsym_pdf ( 'P2Ds' , m_x , m_y , n = 2 ) ,
    sb       = sb ,
    bs       = sb 
    )

model.signal1.sigma.fix ( m.error () )
model.signal2.sigma.fix ( m.error () )
model.signal1.mean .fix ( m.value () )
model.signal2.mean .fix ( m.value () )
model.signal1.mean .fix ( m.value () )
model.signal2.mean .fix ( m.value () )
model.bkg1   .tau  .fix ( 0 )
model.bkg2   .tau  .fix ( 0 )

## fit with fixed mass and sigma
with rooSilent() : 
    result, frame = model. fitTo ( dataset )
    # model.signal1.sigma.release () 
    # model.signal2.sigma.release ()
    # model.signal1.mean .release () 
    # model.signal2.mean .release () 
    result, frame = model. fitTo ( dataset )


if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d '
                   % ( result.status() , result.covQual()  ) )
    print result 

print 'SxS=%s \nSxB=%s \nBxS=%s \nBxB=%s ' % ( result ( model.SS ) [0] ,
                                               result ( model.SB ) [0] ,
                                               result ( model.BS ) [0] ,
                                               result ( model.BB ) [0] ) 
model_5 = model 
models.append ( model ) 


# =============================================================================
## gauss as signal, 1st order polynomial as background 
# =============================================================================
logger.info ('Symmetric non-factorized fit model:  ( Gauss + P1 ) (x) ( Gauss + P1 ) + BBsym' )
sb      = ROOT.RooRealVar('sb','SB',0,10000)
model   = Models.Fit2DSym (
    suffix   = '_6' , 
    signal_1 = signal1  , 
    signal_2 = signal2s , 
    bkg1     = 1 , 
    bkg2D    = Models.PolyPos2Dsym_pdf ( 'P2D5' , m_x , m_y , n = 2 ) ,
    )

model.signal1.sigma.fix ( m.error () )
model.signal2.sigma.fix ( m.error () )
model.signal1.mean .fix ( m.value () )
model.signal2.mean .fix ( m.value () )
model.signal1.mean .fix ( m.value () )
model.signal2.mean .fix ( m.value () )
model.bkg1   .tau  .fix ( 0 )
model.bkg2   .tau  .fix ( 0 )

## fit with fixed mass and sigma
with rooSilent() : 
    result, frame = model. fitTo ( dataset )
    # model.signal1.sigma.release () 
    # model.signal2.sigma.release ()
    # model.signal1.mean .release () 
    # model.signal2.mean .release () 
    result, frame = model. fitTo ( dataset )


if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d '
                   % ( result.status() , result.covQual()  ) )
    print result 

print 'SxS=%s \nSxB=%s \nBxB=%s ' % ( result ( model.SS ) [0] ,
                                      result ( model.SB ) [0] ,
                                      result ( model.BB ) [0] ) 
model_6 = model 
models.append ( model ) 


# =============================================================================
## gauss as signal, expo times 1st order polynomial as background 
# =============================================================================
logger.info ('Non-factorizable background component:  ( Gauss + expo*P1 ) (x) ( Gauss + expo*P1 ) + (expo*P1)**2')
model   = Models.Fit2D (
    suffix   = '_7' , 
    signal_1 = signal1 , 
    signal_2 = signal2 , 
    bkg1     = 1 , 
    bkg2     = 1 ,
    bkg2D    = Models.ExpoPol2D_pdf ( 'P2D7' , m_x , m_y , nx = 1 , ny = 1 ) 
    )

model.signal1.sigma.fix ( m.error () )
model.signal2.sigma.fix ( m.error () )
model.signal1.mean .fix ( m.value () )
model.signal2.mean .fix ( m.value () )
model.signal1.mean .fix ( m.value () )
model.signal2.mean .fix ( m.value () )
model.bkg1   .tau  .fix ( 0 )
model.bkg2   .tau  .fix ( 0 )

## fit with fixed mass and sigma
with rooSilent() : 
    result, frame = model. fitTo ( dataset )
    # model.signal1.sigma.release () 
    # model.signal2.sigma.release ()
    # model.signal1.mean .release () 
    # model.signal2.mean .release () 
    result, frame = model. fitTo ( dataset )
    
    
if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d '
                   % ( result.status() , result.covQual()  ) )
    print result 

print 'SxS=%s \nSxB=%s \nBxS=%s \nBxB=%s ' % ( result ( model.SS ) [0] ,
                                               result ( model.SB ) [0] ,
                                               result ( model.BS ) [0] ,
                                               result ( model.BB ) [0] ) 


model_7 = model 
models.append ( model ) 

# =============================================================================
## gauss as signal, expo times 1st order polynomial as background 
# =============================================================================
logger.info ('Non-factorizable background component:  ( Gauss + expo*P1 ) (x) ( Gauss + expo*P1 ) + Sym(expo*P1)**2')
model   = Models.Fit2D (
    suffix   = '_8' , 
    signal_1 = signal1  , 
    signal_2 = signal2s , 
    bkg1     = 1 , 
    bkg2     = 1 ,
    bkg2D    = Models.ExpoPol2Dsym_pdf ( 'P2D8' , m_x , m_y , n = 1 ) 
    )

model.signal1.sigma.fix ( m.error () )
model.signal1.mean .fix ( m.value () )
model.bkg1   .tau  .fix ( 0 )
model.bkg2   .tau  .fix ( 0 )
model.bkg2D  .tau  .fix ( 0 )

model.SS = 5000
model.BB = 5000
model.SB = 2500
model.BS = 2500

## fit with fixed mass and sigma
with rooSilent() : 
    result, frame = model. fitTo ( dataset )
    # model.signal1.sigma.release () 
    # model.signal2.sigma.release ()
    # model.signal1.mean .release () 
    # model.signal2.mean .release () 
    result, frame = model. fitTo ( dataset )
    
    
if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d '
                   % ( result.status() , result.covQual()  ) )
    print result 

print 'SxS=%s \nSxB=%s \nBxS=%s \nBxB=%s ' % ( result ( model.SS ) [0] ,
                                               result ( model.SB ) [0] ,
                                               result ( model.BS ) [0] ,
                                               result ( model.BB ) [0] ) 

model_8 = model 
models.append ( model ) 
# =============================================================================
## gauss as signal, expo times 1st order polynomial as background 
# =============================================================================
logger.info ('Symmetric fit model with non-factorizable background component:  ( Gauss + expo*P1 ) (x) ( Gauss + expo*P1 ) + Sym(expo*P1)**2')
model   = Models.Fit2DSym (
    suffix   = '_9' , 
    signal_1 = signal1  , 
    signal_2 = signal2s , 
    bkg1     = 1 , 
    bkg2D    = Models.ExpoPol2Dsym_pdf ( 'P2D9' , m_x , m_y , n = 1 ) 
    )

model.signal1.sigma.fix ( m.error () )
model.signal2.sigma.fix ( m.error () )
model.signal1.mean .fix ( m.value () )
model.signal2.mean .fix ( m.value () )
model.signal1.mean .fix ( m.value () )
model.signal2.mean .fix ( m.value () )
model.bkg1   .tau  .fix ( 0 )

model.SS = 5000
model.BB = 5000
model.SB = 5000


## fit with fixed mass and sigma
with rooSilent() : 
    result, frame = model. fitTo ( dataset )
    # model.signal1.sigma.release () 
    # model.signal2.sigma.release ()
    # model.signal1.mean .release () 
    # model.signal2.mean .release () 
    result, frame = model. fitTo ( dataset )
    
    
if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d '
                   % ( result.status() , result.covQual()  ) )
    print result 

print 'SxS=%s \nSxB=%s \nBxB=%s ' % ( result ( model.SS ) [0] ,
                                      result ( model.SB ) [0] ,
                                      result ( model.BB ) [0] ) 


model_9 = model 
models.append ( model ) 

PS = cpp.Gaudi.Math.PhaseSpaceNL( 1.0  , 5.0 , 2 , 5 ) 
# =============================================================================
## gauss as signal, expo times 1st order polynomial as background 
# =============================================================================
logger.info ('Non-factorizable background component:  ( Gauss + expo*P1 ) (x) ( Gauss + expo*P1 ) + (PS*P1)**2')
model   = Models.Fit2D (
    suffix   = '_10' , 
    signal_1 = signal1  , 
    signal_2 = signal2  , 
    bkg1     = 1 , 
    bkg2     = 1 ,
    bkg2D    = Models.PSPol2D_pdf ( 'P2D10' , m_x , m_y , psx = PS , psy = PS , nx = 1 , ny = 1 ) 
    )

model.SS = 5000
model.BB = 5000
model.SB = 2500
model.BS = 2500

model.signal1.sigma.fix ( m.error () )
model.signal2.sigma.fix ( m.error () )
model.signal1.mean .fix ( m.value () )
model.signal2.mean .fix ( m.value () )
model.signal1.mean .fix ( m.value () )
model.signal2.mean .fix ( m.value () )
model.bkg1   .tau  .fix ( 0 )
model.bkg2   .tau  .fix ( 0 )

## fit with fixed mass and sigma
with rooSilent() : 
    result, frame = model. fitTo ( dataset )
    # model.signal1.sigma.release () 
    # model.signal2.sigma.release ()
    # model.signal1.mean .release () 
    # model.signal2.mean .release () 
    result, frame = model. fitTo ( dataset )
    
    
if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d '
                   % ( result.status() , result.covQual()  ) )
    print result 

print 'SxS=%s \nSxB=%s \nBxS=%s \nBxB=%s ' % ( result ( model.SS ) [0] ,
                                               result ( model.SB ) [0] ,
                                               result ( model.BS ) [0] ,
                                               result ( model.BB ) [0] ) 

model_10 = model 
models.append ( model ) 

# =============================================================================
## gauss as signal, expo times 1st order polynomial as background 
# =============================================================================
logger.info ('Non-factorizable symmetric background component:  ( Gauss + expo*P1 ) (x) ( Gauss + expo*P1 ) + (PS*P1)**2')
model   = Models.Fit2D (
    suffix   = '_11' , 
    signal_1 = signal1  , 
    signal_2 = signal2s , 
    bkg1   = 1 , 
    bkg2   = 1 ,
    bkg2D    = Models.PSPol2Dsym_pdf ( 'P2D11' , m_x , m_y , ps = PS , n = 1 ) 
    )

model.signal1.sigma.fix ( m.error () )
model.signal2.sigma.fix ( m.error () )
model.signal1.mean .fix ( m.value () )
model.signal2.mean .fix ( m.value () )
model.signal1.mean .fix ( m.value () )
model.signal2.mean .fix ( m.value () )
model.bkg1   .tau  .fix ( 0 )
model.bkg2   .tau  .fix ( 0 )

model.SS = 5000
model.BB = 5000
model.SB = 2500
model.BS = 2500

## fit with fixed mass and sigma
with rooSilent() : 
    result, frame = model. fitTo ( dataset )
    # model.signal1.sigma.release () 
    # model.signal2.sigma.release ()
    # model.signal1.mean .release () 
    # model.signal2.mean .release () 
    result, frame = model. fitTo ( dataset )
    
    
if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d '
                   % ( result.status() , result.covQual()  ) )
    print result 

print 'SxS=%s \nSxB=%s \nBxS=%s \nBxB=%s ' % ( result ( model.SS ) [0] ,
                                               result ( model.SB ) [0] ,
                                               result ( model.BS ) [0] ,
                                               result ( model.BB ) [0] ) 


model_11 = model 
models.append ( model ) 

# =============================================================================
## gauss as signal, expo times 1st order polynomial as background 
# =============================================================================
logger.info ('Symmetric fit model with non-factorizable background component:  ( Gauss + expo*P1 ) (x) ( Gauss + expo*P1 ) + (PS*P1)**2')
model   = Models.Fit2DSym (
    suffix   = '_12' , 
    signal_1 = signal1  , 
    signal_2 = signal2s , 
    bkg1     = 1 , 
    bkg2D    = Models.PSPol2Dsym_pdf ( 'P2D12' , m_x , m_y , ps = PS , n = 1 ) 
    )

model.SS = 5000
model.BB = 5000
model.SB = 5000


model.signal1.sigma.fix ( m.error () )
model.signal2.sigma.fix ( m.error () )
model.signal1.mean .fix ( m.value () )
model.signal2.mean .fix ( m.value () )
model.signal1.mean .fix ( m.value () )
model.signal2.mean .fix ( m.value () )
model.bkg1   .tau  .fix ( 0 )
model.bkg2   .tau  .fix ( 0 )

## fit with fixed mass and sigma
with rooSilent() : 
    result, frame = model. fitTo ( dataset )
    # model.signal1.sigma.release () 
    # model.signal2.sigma.release ()
    # model.signal1.mean .release () 
    # model.signal2.mean .release () 
    result, frame = model. fitTo ( dataset )
    
    
if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d '
                   % ( result.status() , result.covQual()  ) )
    print result 

print 'SxS=%s \nSxB=%s \nBxB=%s ' % ( result ( model.SS ) [0] ,
                                      result ( model.SB ) [0] ,
                                      result ( model.BB ) [0] ) 

model_12 = model 
models.append ( model ) 


# =============================================================================
## gauss as signal, expo times 1st order polynomial as background 
# =============================================================================
logger.info ('Non-factorizable fit component:  ( Gauss + expo*P1 ) (x) ( Gauss + expo*P1 ) + (Expo*PS)**2')
model   = Models.Fit2D (
    suffix   = '_13' , 
    signal_1 = signal1  , 
    signal_2 = signal2  , 
    bkg1   = 1 , 
    bkg2   = 1 ,
    bkg2D    = Models.ExpoPSPol2D_pdf ( 'P2D13' , m_x , m_y , psy = PS , nx = 1 , ny = 1 ) 
    )
model.SS = 5000
model.BB = 5000
model.SB = 2500
model.BS = 2500

model.signal1.sigma.fix ( m.error () )
model.signal2.sigma.fix ( m.error () )
model.signal1.mean .fix ( m.value () )
model.signal2.mean .fix ( m.value () )
model.signal1.mean .fix ( m.value () )
model.signal2.mean .fix ( m.value () )
model.bkg1   .tau  .fix ( 0 )
model.bkg2   .tau  .fix ( 0 )

## fit with fixed mass and sigma
with rooSilent() : 
    result, frame = model. fitTo ( dataset )
    # model.signal1.sigma.release () 
    # model.signal2.sigma.release ()
    # model.signal1.mean .release () 
    # model.signal2.mean .release () 
    result, frame = model. fitTo ( dataset )
    
    
if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d '
                   % ( result.status() , result.covQual()  ) )
    print result 

print 'SxS=%s \nSxB=%s \nBxS=%s \nBxB=%s ' % ( result ( model.SS ) [0] ,
                                               result ( model.SB ) [0] ,
                                               result ( model.BS ) [0] ,
                                               result ( model.BB ) [0] ) 

model_13 = model 
models.append ( model ) 


knots = cpp.std.vector('double')()
knots.push_back(      m_x.xmin()              )
knots.push_back( 0.5*(m_x.xmin()+m_x.xmax() ) )
knots.push_back(                 m_x.xmax()   )
spline1 = cpp.Gaudi.Math.BSpline( knots , 2 )
SPLINE  = cpp.Gaudi.Math.Spline2D ( spline1 , spline1 ) 

# =============================================================================
## gauss as signal, expo times 1st order polynomial as background 
# =============================================================================
logger.info ('Non-factorizable background component (spline):  ( Gauss + expo*P1 ) (x) ( Gauss + expo*P1 ) + Spline2D')
model   = Models.Fit2D (
    suffix   = '_14' , 
    signal_1 = signal1  , 
    signal_2 = signal2  , 
    bkg1   = 1 , 
    bkg2   = 1 ,
    bkg2D    = Models.Spline2D_pdf ( 'P2D14' , m_x , m_y , spline = SPLINE ) 
    )
model.SS = 5000
model.BB = 5000
model.SB = 2500
model.BS = 2500

model.signal1.sigma.fix ( m.error () )
model.signal2.sigma.fix ( m.error () )
model.signal1.mean .fix ( m.value () )
model.signal2.mean .fix ( m.value () )
model.signal1.mean .fix ( m.value () )
model.signal2.mean .fix ( m.value () )
model.bkg1   .tau  .fix ( 0 )
model.bkg2   .tau  .fix ( 0 )

## fit with fixed mass and sigma
with rooSilent() : 
    result, frame = model. fitTo ( dataset )
    # model.signal1.sigma.release () 
    # model.signal2.sigma.release ()
    # model.signal1.mean .release () 
    # model.signal2.mean .release () 
    result, frame = model. fitTo ( dataset )
    
    
if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d '
                   % ( result.status() , result.covQual()  ) )
    print result 

print 'SxS=%s \nSxB=%s \nBxS=%s \nBxB=%s ' % ( result ( model.SS ) [0] ,
                                               result ( model.SB ) [0] ,
                                               result ( model.BS ) [0] ,
                                               result ( model.BB ) [0] ) 


model_14 = model 
models.append ( model ) 

SPLINES = cpp.Gaudi.Math.Spline2DSym ( spline1 ) 
# =============================================================================
## gauss as signal, expo times 1st order polynomial as background 
# =============================================================================
logger.info ('Non-factorized symmetric background component (spline):  ( Gauss + expo*P1 ) (x) ( Gauss + expo*P1 ) + Spline2Dsym')
model   = Models.Fit2D (
    suffix   = '_15'    ,
    signal_1 = signal1  , 
    signal_2 = signal2s , 
    bkg1   = 1 , 
    bkg2   = 1 ,
    bkg2D    = Models.Spline2Dsym_pdf ( 'P2D15' , m_x , m_y , spline = SPLINES ) 
    )

model.SS = 5000
model.BB = 5000
model.SB = 2500
model.BS = 2500

model.signal1.sigma.fix ( m.error () )
model.signal2.sigma.fix ( m.error () )
model.signal1.mean .fix ( m.value () )
model.signal2.mean .fix ( m.value () )
model.signal1.mean .fix ( m.value () )
model.signal2.mean .fix ( m.value () )
model.bkg1   .tau  .fix ( 0 )
model.bkg2   .tau  .fix ( 0 )

## fit with fixed mass and sigma
with rooSilent() : 
    result, frame = model. fitTo ( dataset )
    # model.signal1.sigma.release () 
    # model.signal2.sigma.release ()
    # model.signal1.mean .release () 
    # model.signal2.mean .release () 
    result, frame = model. fitTo ( dataset )
    
    
if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d '
                   % ( result.status() , result.covQual()  ) )
    print result 

print 'SxS=%s \nSxB=%s \nBxS=%s \nBxB=%s ' % ( result ( model.SS ) [0] ,
                                               result ( model.SB ) [0] ,
                                               result ( model.BS ) [0] ,
                                               result ( model.BB ) [0] ) 

model_15 = model 
models.append ( model ) 


# =============================================================================
## gauss as signal, expo times 1st order polynomial as background 
# =============================================================================
logger.info ('Symmetric fit model with non-factorizable symmetric (spline) background component:  ( Gauss + expo*P1 ) (x) ( Gauss + expo*P1 ) + Spline2Dsym')
model   = Models.Fit2DSym (
    suffix   = '_16' , 
    signal_1 = signal1  , 
    signal_2 = signal2s , 
    bkg1     = 1 , 
    bkg2D    = Models.Spline2Dsym_pdf ( 'P2D16' , m_x , m_y , spline = SPLINES ) 
    )

model.SS = 5000
model.BB = 5000
model.SB = 5000

model.signal1.sigma.fix ( m.error () )
model.signal2.sigma.fix ( m.error () )
model.signal1.mean .fix ( m.value () )
model.signal2.mean .fix ( m.value () )
model.signal1.mean .fix ( m.value () )
model.signal2.mean .fix ( m.value () )
model.bkg1   .tau  .fix ( 0 )

## fit with fixed mass and sigma
with rooSilent() : 
    result, frame = model. fitTo ( dataset )
    # model.signal1.sigma.release () 
    # model.signal2.sigma.release ()
    # model.signal1.mean .release () 
    # model.signal2.mean .release () 
    result, frame = model. fitTo ( dataset )
    
    
if 0 != result.status() or 3 != result.covQual() :
    logger.warning('Fit is not perfect MIGRAD=%d QUAL=%d '
                   % ( result.status() , result.covQual()  ) )
    print result 

print 'SxS=%s \nSxB=%s \nBxB=%s ' % ( result ( model.SS ) [0] ,
                                      result ( model.SB ) [0] ,
                                      result ( model.BB ) [0] ) 


model_16 = model 
models.append ( model ) 


"""
#
## check that everything is serializable
# 
import Ostap.ZipShelve   as DBASE
with DBASE.tmpdb() as db : 
    db['x,y,vars'] = m_x, m_y, varset
    db['dataset' ] = dataset
    db['models'  ] = models
    db['result'  ] = result
    db['frame'   ] = frame
    db.ls() 
"""


# =============================================================================
# The END 
# =============================================================================
