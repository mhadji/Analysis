#ifndef MCPARTICLEARRAYFILTERALG_H
#define MCPARTICLEARRAYFILTERALG_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

class IMCParticleArrayFilter;
struct IPrintMCDecayTreeTool;
namespace LHCb {
  class IParticlePropertySvc;
}

/** @class MCParticleArrayFilterAlg MCParticleArrayFilterAlg.h
 *
 *  Simple algorithm that takes a container of LHCb::MCParticle from the
 *  Transient Event Store (TES), applies an IMCParticleArrayFilter to produce
 *  an LHCb::MCParticle container of filtered MCParticles, and places this in
 *  the TES.
 *  <b>Options</b>:
 *  InputLocation: the TES location of the input LHCb::MCParticles
 *  (default LHCb::MCParticleLocation::Default)
 *  OutputLocation: the TES location where the LHCb::MCParticle container of
 *  filtered particles is placed.
 *  IMCParticleArrayFilter: Name of the implementation of the
 *  IMCParticleArrayFilter interface to be used.
 *  @see IMCParticleArrayFilter
 *
 *  <b>Example:</b>: Take a container of MCParticles from
 *  "/Event/MC/MyParticles", filter them according to a decay descriptor,
 *  and place the container of filtered MCParticles in
 *  "/Event/MC/MyDecayHeads".
 *
 *  @code
 *
 *  @endcode
 *
 *  @author Juan PALACIOS
 *  @date   2008-04-09
 */
class MCParticleArrayFilterAlg : public GaudiAlgorithm {
public:
  /// Standard constructor
  MCParticleArrayFilterAlg( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution

protected:

private:

  IMCParticleArrayFilter* m_filter = nullptr;
  std::string m_filterType;
  std::string m_inputLocation;
  std::string m_outputLocation;
  LHCb::IParticlePropertySvc* m_ppSvc = nullptr;

};
#endif // MCPARTICLEARRAYFILTERALG_H
