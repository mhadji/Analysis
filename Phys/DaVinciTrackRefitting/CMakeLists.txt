################################################################################
# Package: DaVinciTrackRefitting
################################################################################
gaudi_subdir(DaVinciTrackRefitting)

gaudi_depends_on_subdirs(Event/TrackEvent
                         Kernel/LHCbKernel
                         Phys/DaVinciKernel
                         Phys/DecayTreeFitter
                         Phys/LoKiCore
                         Phys/LoKiPhys
                         Tr/TrackFitEvent
                         Tr/TrackKernel)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${ROOT_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS})

gaudi_add_module(DaVinciTrackFitting
                 src/*.cpp
                 LINK_LIBRARIES TrackEvent LHCbKernel DaVinciKernelLib DecayTreeFitter LoKiCoreLib LoKiPhysLib TrackFitEvent TrackKernel)

gaudi_install_python_modules()
