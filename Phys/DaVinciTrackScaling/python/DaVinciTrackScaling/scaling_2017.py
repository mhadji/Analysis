#!/usr/bin/env python
# =============================================================================
# @file scaling_2017.py
# Momentum scaling for 2017
# @author Matt  NEEDHAM 
# @author Vanya BELYAEV Ivan.Belyaev@itep.ru
# @date   2018-01-31
# ============================================================================
"""Momentum scaling fro 2017

Input data are specified close to the start of the script

- input ROOT file with historams
the file should have two 2D-histograms, named as 'idp-plus' and 'idp-minus'

- global delta
- list of run-dependent offsets as list of triplets:  [ (start,stop,offset) ],
where (start,stop) represent the run range (both edges are inclusive) 

As the output xml-file MomentumScale.xml is generated in cwd

"""
# ============================================================================
__author__  = "Vanya BELYAEV Ivan.Belyaev@itep.ru"
__date__    = "2018-01-31"
__version__ = ""
# ============================================================================
import ROOT, Ostap.PyRoUts  
# ============================================================================
from momentum_scale import buildXML, logger  
# ============================================================================
logger.info(100*'*')
logger.info(__doc__)
logger.info(100*'*')
# ============================================================================

# ============================================================================
## the histograms 
with ROOT.TFile('../../data/BFitResults-fine-17.root','READ') as the_file :
    idp_plus  = the_file.Get('idp-plus' ).clone()
    idp_minus = the_file.Get('idp-minus').clone() 
    logger.info ( 'HISTOS are read from %s'  % the_file ) 

# =============================================================================
## global delta
## delta =  -0.19e-3
## delta = -0.143e-3
delta = -0.155e-3
logger.info ( 'Global delta: %s' % delta)


## if (run >= 190000   && run <=  193500 ) alpha =0.198731e-3 ;
## else if (run >= 193500   && run <=  193800 ) alpha = 0.266449e-3 ;
## else if (run >= 193801   && run <=  194000 ) alpha = 0.238547e-3 ;
## else if (run >= 194000   && run <=  194400 ) alpha = 0.269386e-3 ;

## // down
## else if (run >= 194700   && run <=  195700 ) alpha = 0.0585973e-3;
## else if (run >= 195900   && run <=  196000 ) alpha = 0.105339e-3;
## else if (run >= 196001   && run <=  196500 ) alpha = 0.111267e-3;
## else if (run >= 196501   && run <=  196750 ) alpha = 0.075941e-3;
## else if (run >= 196751   && run <=  196850 ) alpha = 0.108717e-3;
## else if (run >= 196851   && run <=  197000 ) alpha = 0.0885255e-3;

## // up
## else if (run >= 197001   && run <=  197500 ) alpha = 0.211224e-3;
## else if (run >= 197501   && run <=  197800 ) alpha = 0.208363e-3;
## else if (run >= 197800   && run <=  198500 ) alpha = 0.262614e-3;
## else if (run >= 198501   && run <=  199200 ) alpha = 0.271458e-3;

## // down
## else if (run >= 199400   && run <=  200400 ) alpha = 0.0991116e-3;
## else if (run >= 200401   && run <= 201200  ) alpha = 0.0972021e-3;

## // up
## else if (run >=  201201  && run <=  201600 ) alpha = 0.261225e-3;
## else if (run >=  201601  && run <=  202100 ) alpha = 0.252726e-3;
## else if (run >=  202600 && run <=  203000 ) alpha = 0.185992e-3;

offsets = [
    ( 190000 , 193500 , 0.198731e-3  ) , 
    ( 193500 , 193800 , 0.266449e-3  ) ,
    ( 193801 , 194000 , 0.238547e-3  ) , 
    ( 194000 , 194400 , 0.269386e-3  ) ,     
    ## // down
    ( 194700 , 195700 , 0.0585973e-3 ) ,
    ( 195900 , 196000 , 0.105339e-3  ) , 
    ( 196001 , 196500 , 0.111267e-3  ) , 
    ( 196501 , 196750 , 0.075941e-3  ) , 
    ( 196751 , 196850 , 0.108717e-3  ) , 
    ( 196851 , 197000 , 0.0885255e-3 ) ,     
    ## // up
    ( 197001 , 197500 , 0.211224e-3  ) , 
    ( 197501 , 197800 , 0.208363e-3  ) , 
    ( 197800 , 198500 , 0.262614e-3  ) , 
    ( 198501 , 199200 , 0.271458e-3  ) , 
    ## // down
    ( 199400 , 200400 , 0.0991116e-3 ) , 
    ( 200401 , 201200 , 0.0972021e-3 ) , 
    ## // up
    ( 201201 , 201600 , 0.261225e-3  ) , 
    ( 201601 , 202100 , 0.252726e-3  ) , 
    ( 202600 , 203000 , 0.185992e-3  ) ]


# =============================================================================
# Build XML-document 
# =============================================================================
buildXML ( year      = '2k+17'    ,
           reco      = 'Reco17'   ,
           idp_plus  = idp_plus   ,
           idp_minus = idp_minus  ,
           offsets   = offsets    ,
           delta     = delta      ) 

# ============================================================================
# The END 
# ============================================================================

